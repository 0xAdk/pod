# macOS Detailed Instructions

## Notes
These instructions may not be correct as they have not been tested. There might be missing or incorrect steps. Use them only as a guideline.

## Check macOS Version
You should check your macOS version before attempting to compile the launcher.
If your version is above macOS 10.14, then wine can't run (read further for alternatives), which means that the launcher is of little use. You can use the instructions for later macOS versions provided here, though they might not work.

## Getting Qt
If you don't have it already, you should install [homebrew](https://brew.sh/).
Then from a terminal, run:
```bash
brew install qt
```

## Getting Wine (< macOS 10.15)
You should install wine in order to run RoRML.
Go to [WineHQ](https://wiki.winehq.org/MacOS) for official instructions on installing wine.

## Getting Wine (>= macOS 10.15)
[macports-wine](https://github.com/Gcenx/macports-wine)

## Compiling
Open a terminal at the project root (should be the folder named pod) and run:
```bash
qmake && make
```
