#!/bin/sh

set -e

git submodule update --init
cmake -S . -B build
cmake --build build --parallel "$(nproc)"
