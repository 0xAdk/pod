#!/bin/sh

cmd='build'
[ -n "${2}" ] && cmd="${2}"
[ -n "${1}" ] && output="#${1}"
nix --extra-experimental-features nix-command --extra-experimental-features flakes "${cmd}" --no-update-lock-file --no-write-lock-file './?submodules=1'"${output}"
