#include <rainfusion.h>
#include <links.h>

#include <yaml-cpp/yaml.h>

#include <regex>
#include <algorithm>

#include <QString>
#include <QByteArray>

std::regex const space_regex("[[:space:]]+");
std::regex const clean_regex("[^[:alpha:]^[:space:]^_]+");
std::string generateDownloadLink(std::string const & uuid, std::string const & name, std::string const & version) {
	std::string file_name = name;
	file_name = std::string(std::regex_replace(file_name, clean_regex, ""));
	file_name = std::string(std::regex_replace(file_name, space_regex, "_"));
	std::transform(file_name.begin(), file_name.end(), file_name.begin(), ::tolower);
	return links::RAINFUSION_CDN_MOD + uuid + "/" + file_name + "_" + version + ".zip";
}

// TODO Unify with mods.cpp
std::string getYAMLString(YAML::Node const & node, std::string const & name) {
	std::string value;
	if (node[name])
		value = node[name].as<std::string>();
	return value;
}

namespace rainfusion {
	std::string RainfusionMod::getInfo(mod::INFO info_type) const {
		switch (info_type) {
			case mod::ID:
				return uuid;
			case mod::NAME:
				return name;
			case mod::VERSION:
				return version.toString();
			case mod::AUTHOR:
				return author;
			case mod::TYPE:
				return type;
			case mod::SUMMARY:
				return summary;
			case mod::DESCRIPTION:
				return description;
		}
		return "\0";
	}

	mod::TEXT_TYPE RainfusionMod::getTextType(mod::INFO info_type) const {
		if (info_type == mod::DESCRIPTION)
			return mod::MARKDOWN;
		return mod::PLAIN;
	}

	RainfusionMod parseMod(YAML::Node const & node) {
		RainfusionMod data;

		data.uuid = getYAMLString(node, "id");
		data.name = getYAMLString(node, "name");

		for (auto const & author_node : node["author"]) {
			if (!data.author.empty())
				data.author += " & ";
			data.author += getYAMLString(author_node, "username");
		}

		data.summary = getYAMLString(node, "summary");

		data.description =
		QString(
			QByteArray::fromBase64(
				QString::fromStdString(
					getYAMLString(node, "description")
				).toUtf8()
			)
		).toStdString();

		data.version = getYAMLString(node, "version");

		// Not using data.version.toString() since it changes the format
		data.download = generateDownloadLink(data.uuid, data.name, getYAMLString(node, "version"));

		data.type = getYAMLString(node, "type");

		for (auto const & tag: node["tags"]) {
			std::string tag_string = tag.as<std::string>();
			data.tags.push_back(tag_string);
		}

		return data;
	}

	std::vector<RainfusionMod> parseMods(std::string mods_html) {
		std::vector<RainfusionMod> mods;

		YAML::Node mods_root = YAML::Load(mods_html);
		for (auto const & mod_node : mods_root) {
			RainfusionMod mod = parseMod(mod_node);
			mods.push_back(mod);
		}

		return mods;
	}
}
