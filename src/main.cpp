#include <global.h>
#include <launcher.h>

#include <QApplication>

int main(int argc, char * * argv) {
	// Application
	QApplication app(argc, argv);

	// Getting and saving binary path
	global::setApplicationDirectory(app.applicationFilePath());
	qInfo() << "Application path is " << global::d_application;

	// Creating settings (ini to avoid registry on windows)
	global::settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, "pod");

	//QCoreApplication::setOrganizationName("");
	//QCoreApplication::setOrganizationDomain("");
	QCoreApplication::setApplicationName("pod");

	// Run launcher
	Launcher launcher;
	launcher.show();
	return app.exec();
}
