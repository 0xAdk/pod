#ifndef MODLISTMODEL_H
#define MODLISTMODEL_H

#include <mod.h>

#include <vector>

#include <QAbstractListModel>

class ModListModel : public QAbstractListModel {
	Q_OBJECT

	public:
		ModListModel(QObject * parent = nullptr);
		ModListModel(std::vector<mod::ModPath> const & mods, QObject * parent = nullptr);

		int rowCount(QModelIndex const & parent = QModelIndex()) const override;
		QVariant data(QModelIndex const & index, int role) const override;
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
		Qt::ItemFlags flags(QModelIndex const & index) const;

		void setMods(std::vector<mod::ModPath> mods, std::vector<bool> enabled = std::vector<bool>());

	private:
		std::vector<bool> enabled;
		std::vector<mod::ModPath> mods;
};

#endif //MODLISTMODEL_H
