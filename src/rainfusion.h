#ifndef RAINFUSION_H
#define RAINFUSION_H

#include <mod.h>

#include <string>
#include <vector>

namespace rainfusion {
	class RainfusionMod : public mod::IMod {
		public:
			std::string getInfo(mod::INFO info_type) const override;
			mod::TEXT_TYPE getTextType(mod::INFO info_type) const override;

			std::string uuid;
			std::string name;
			version::SemanticVersion version;
			std::string author;
			std::string type;
			std::string summary;
			std::string description;
			std::string icon;
			std::string download;
			//std::vector<RainfusionMod> dependencies;
			std::vector<std::string> tags;
	};

	std::vector<RainfusionMod> parseMods(std::string mods_html);
}

#endif //RAINFUSION_H
