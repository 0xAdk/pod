#ifndef PREFIXMANAGER_H
#define PREFIXMANAGER_H

#include <taskinfo.h>
#include <prefix.h>

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QBuffer>

class PrefixManager : public QObject {
	struct PrefixUpdateInfo {
		QString d_prefix = "\0";
		bool skip_version = false;
		TaskInfo * task = nullptr;
		QWidget * widget = nullptr;
	};

	Q_OBJECT

	public:
		PrefixManager();
		~PrefixManager();

		bool valid();
		bool validVanilla();

		bool installBuffer(QBuffer * modloader, QString d_prefix, TaskInfo * taskinfo = nullptr);
		//bool installZip(QString f_zip, QString d_prefix, TaskInfo * taskinfo = nullptr);
		bool installUrl(QUrl url, QString d_prefix, PrefixUpdateInfo * info = nullptr);
		bool installLink(QString link, QString d_prefix, PrefixUpdateInfo * info = nullptr);
		bool installRainfusion(QString d_prefix, bool beta = false, bool skip_version = false, QWidget * widget = nullptr);

		QString getVanillaDirectory();
		bool setVanillaDirectory(QString d_vanilla);
		bool detectVanillaDirectory();
		bool grabVanillaDirectory(QWidget * widget);

		void downloadModloaderFinished();
		void downloadModloaderError(QNetworkReply::NetworkError code);
		void downloadModloaderProgress(qint64 received, qint64 total);

		void downloadRainfusionFinished();
		void downloadRainfusionError(QNetworkReply::NetworkError code);
		void downloadRainfusionProgress(qint64 received, qint64 total);

		std::vector<prefix::Prefix> getPrefixes();
		bool addPrefix(prefix::Prefix entry);
		void detectPrefixes();

	signals:
		void taskAdded(TaskInfo * info);
		void changedPrefixes();
		void changedVanilla();

	private:
		void cancelDownload(QNetworkReply * reply);

		std::vector<prefix::Prefix> prefixes;
		QString d_vanilla = "\0";
		QNetworkAccessManager * netman = nullptr;
		std::map<QNetworkReply *, PrefixUpdateInfo> ongoing_updates;
};

#endif //PREFIXMANAGER_H
