#ifndef TASKINFO_H
#define TASKINFO_H

#include <mutex>

#include <QObject>

enum TaskState {
	NOSTATE,
	RUNNING,
	SUCCESS,
	FAILURE,
	CANCEL
};

class TaskInfo : public QObject {
	Q_OBJECT

	public:

		TaskInfo();

		bool getCompleted();
		TaskState getState();
		int getProgress();
		QString getText();

		void setCompleted(bool completed);
		void setState(TaskState state);
		void setProgress(int progress);
		void setText(QString text);
		bool shouldCancel();

	signals:
		void progressChanged();
		void completed();

	private:
		std::mutex lock;
		TaskState state = NOSTATE;
		int progress = 0;
		QString text = "\0";
};

#endif //TASKINFO_H
