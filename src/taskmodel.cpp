#include <taskmodel.h>

#include <QtConcurrent>
#include <QApplication>


TaskModel::TaskModel(QObject * parent) : QAbstractItemModel(parent) {}

QModelIndex TaskModel::index(int row, int column, QModelIndex const & parent) const {
	QModelIndex index = createIndex(row, column);
	return index;
}

QModelIndex TaskModel::parent(QModelIndex const & index) const {
	return createIndex(0, 0);
}

int TaskModel::rowCount(QModelIndex const & parent) const {
	return tasks.size();
}

int TaskModel::columnCount(QModelIndex const & parent) const {
	return 2;
}

QVariant TaskModel::data(QModelIndex const & index, int role) const {
	if (role == Qt::DisplayRole) {
		if (index.column() == 1)
			return QVariant(tasks.at(index.row())->getProgress());
		else
			return QVariant(tasks.at(index.row())->getText());
	}
	return QVariant();
}

QVariant TaskModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if (role == Qt::DisplayRole)
		return section == 0 ? QVariant("Task") : QVariant("Progress");
	return QVariant();
}

/*
template <class C> TaskInfo * TaskModel::addTask(void (C::*task)(TaskInfo *), C * object) {
	emit layoutAboutToBeChanged();

	TaskInfo * taskinfo = new TaskInfo;
	connect(taskinfo, SIGNAL(progressChanged()), this, SLOT(progressUpdate()));
	QtConcurrent::run(object, task, taskinfo);
	tasks.push_back(taskinfo);

	emit layoutChanged();

	return taskinfo;
}

TaskInfo * TaskModel::addTask(void task(TaskInfo *)) {
	emit layoutAboutToBeChanged();

	TaskInfo * taskinfo = new TaskInfo;
	connect(taskinfo, SIGNAL(progressChanged()), this, SLOT(progressUpdate()));
	QtConcurrent::run(task, taskinfo);
	tasks.push_back(taskinfo);

	emit layoutChanged();
	return taskinfo;
}
*/

TaskInfo * TaskModel::addTask(TaskInfo * taskinfo) {
	emit layoutAboutToBeChanged();

	connect(taskinfo, SIGNAL(progressChanged()), this, SLOT(progressUpdate()));
	tasks.push_back(taskinfo);

	emit layoutChanged();
	return taskinfo;
}

void TaskModel::wait() {
	while (tasks.size() > 0);
	return;
}

void TaskModel::progressUpdate() {
	TaskInfo * task = qobject_cast<TaskInfo *>(sender());

	int task_index = 0;
	for (int i = 0; i < tasks.size(); i++) {
		if (tasks.at(i) == task) {
			task_index = i;
			break;
		}
	}

	emit dataChanged(index(task_index, 0), index(task_index, 1));
}

void TaskModel::clearCompleted() {
	emit layoutAboutToBeChanged();

	QMutableListIterator<TaskInfo *> it(tasks);
	while(it.hasNext()) {
		TaskInfo * info = it.next();
		if (info->getCompleted()) {
			it.remove();
			delete info;
		}
	}
	
	emit layoutChanged();
}

TaskModel::~TaskModel() {
	for (int i = 0; i < tasks.size(); i++) {
		tasks.at(i)->setState(CANCEL);
	}
}

void TaskViewDelegate::paint(QPainter * painter, QStyleOptionViewItem const & option, QModelIndex const & index) const {
	if (index.column() != 1) {
		QItemDelegate::paint(painter, option, index);
		return;
	}

	QStyleOptionProgressBar bar;
	bar.state = QStyle::State_Enabled;
	bar.rect = option.rect;
        bar.minimum = 0;
        bar.maximum = 100;
        bar.progress = index.data().toInt();

        QApplication::style()->drawControl(QStyle::CE_ProgressBar, &bar, painter);
}
