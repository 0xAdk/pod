#include <prefix.h>

#include <QDir>
#include <QFile>
#include <QSettings>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

namespace prefix {
	bool isPrefixDirectory(std::string directory) {
		QString d_prefix = QString::fromStdString(directory);
		// TODO
		if (QDir(d_prefix).exists() && QFile(d_prefix + "/RoRML.exe").exists()) {
			return true;
		}
		return false;
	}

	profile::ProfileFile getProfile(std::string d_prefix, std::string profile_name) {
		return profile::ProfileFile(QString::fromStdString(d_prefix + "/profiles/" + profile_name) + "/profile.json", true);
	}

	bool setCurrentProfile(std::string d_prefix, std::string profile_name) {
		QSettings launcher_settings(QString::fromStdString(d_prefix) + "/launchersettings.ini", QSettings::IniFormat);
		launcher_settings.beginGroup("launcher");
		launcher_settings.setValue("profile", QString::fromStdString(profile_name));
		launcher_settings.endGroup();
		return true;
	}

	std::vector<profile::ProfileFile> getProfiles(std::string d_prefix) {
		std::vector<profile::ProfileFile> profiles;

		QString path = QString::fromStdString(d_prefix) + "/profiles";

		QStringList profile_dirs = QDir(path).entryList(QStringList(), QDir::Dirs);
		for (int i = 0; i < profile_dirs.size(); i++) {
			QString profile_dir = profile_dirs.at(i);
			if (profile_dir == "." || profile_dir == "..")
				continue;
			profile_dir = path + "/" + profile_dir;
			if (profile::isProfileDirectory(profile_dir.toStdString())) {
				profiles.push_back(profile::ProfileFile(profile_dir + "/profile.json", true));
			}
		}

		return profiles;
	}

	Prefix::Prefix() {}

	Prefix::Prefix(std::string path) {
		setPath(path);
	}

	bool Prefix::setPath(std::string path) {
		if (isPrefixDirectory(path)) {
			this->path = path;
			return true;
		}
		return false;
	}

	bool Prefix::valid() const {
		return isPrefixDirectory(path);
	}

	bool Prefix::refresh() {
		return readProfiles();
	}

	bool Prefix::readProfiles() {
		profiles = prefix::getProfiles(path);
		// TODO
		return true;
	}

	bool createProfile(std::string d_prefix, std::string profile_name) {
		if (!isPrefixDirectory(d_prefix))
			return false;

		std::string d_profile = d_prefix + "/profiles/" + profile_name;

		if (profile::isProfileDirectory(d_profile))
			return true;

		if (!QDir().mkpath(QString::fromStdString(d_profile)))
			return false;

		QFile profile(QString::fromStdString(d_profile) + "/profile.json");

		if (!profile.open(QIODevice::WriteOnly))
			return false;

		QJsonObject json;
		json["mods"] = QJsonArray();
		QJsonDocument document(json);
		profile.write(document.toJson());
		profile.close();

		return true;
	}

	bool deleteProfile(std::string d_prefix, std::string profile_name) {
		std::string d_profile = d_prefix + "/profiles/" + profile_name;

		if (!profile::isProfileDirectory(d_profile))
			return false;

		return QDir(QString::fromStdString(d_profile)).removeRecursively();
	}

	profile::ProfileFile getCurrentProfile(std::string d_prefix) {
		QString qd_prefix = QString::fromStdString(d_prefix);
		QSettings launcher_settings(qd_prefix + "/launchersettings.ini", QSettings::IniFormat);
		launcher_settings.beginGroup("launcher");
		QString profile_name = launcher_settings.value("profile").toString();
		launcher_settings.endGroup();

		return profile::ProfileFile(qd_prefix + "/profiles/" + profile_name + "/profile.json");
	}

	bool Prefix::createProfile(std::string profile_name) {
		return prefix::createProfile(path, profile_name);
	}

	bool Prefix::deleteProfile(std::string profile_name) {
		return prefix::deleteProfile(path, profile_name);
	}

	profile::ProfileFile Prefix::getCurrentProfile() {
		return prefix::getCurrentProfile(path);
	}

	std::string getPrefixVersionString(std::string d_prefix) {
		QString qd_prefix = QString::fromStdString(d_prefix);

		if (!isPrefixDirectory(d_prefix))
			return "\0";

		QFile file(qd_prefix + "/resources/version.json");
		if (!file.open(QIODevice::ReadOnly))
			return "\0";

		QJsonObject object = QJsonDocument::fromJson(file.readAll()).object();
		return object["master_version"].toString().toStdString();
	}
}
