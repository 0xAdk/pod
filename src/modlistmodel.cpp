#include <modlistmodel.h>

ModListModel::ModListModel(QObject * parent) : QAbstractListModel(parent) {}

ModListModel::ModListModel(std::vector<mod::ModPath> const & mods, QObject * parent) : QAbstractListModel(parent) {
	this->mods = mods;
}

int ModListModel::rowCount(QModelIndex const & parent) const {
	return mods.size();
}

QVariant ModListModel::data(QModelIndex const & index, int role) const {
	if (!index.isValid())
		return QVariant();

	if (index.row() >= mods.size())
		return QVariant();

	if (role == Qt::CheckStateRole)
		return enabled.at(index.row()) ? Qt::Checked : Qt::Unchecked;

	if (role == Qt::DisplayRole)
		return QString::fromStdString(mods.at(index.row()).name);
	else
		return QVariant();
}

QVariant ModListModel::headerData(int section, Qt::Orientation orientation, int role) const {
	if (role != Qt::DisplayRole)
		return QVariant();
	return QVariant("Header");
	if (role == Qt::DisplayRole) {
		if (orientation == Qt::Horizontal)
			return QStringLiteral("C%1").arg(section);
		else
			return QStringLiteral("R%1").arg(section);
	}
}

Qt::ItemFlags ModListModel::flags(QModelIndex const & index) const {
	Qt::ItemFlags default_flags = QAbstractListModel::flags(index);
	if (index.isValid())
		return default_flags | Qt::ItemIsUserCheckable;
	return default_flags;
}

void ModListModel::setMods(std::vector<mod::ModPath> mods, std::vector<bool> enabled) {
	emit layoutAboutToBeChanged();
	this->mods = mods;
	if (mods.size() == enabled.size()) {
		this->enabled = enabled;
	}
	else {
		this->enabled = std::vector<bool>();
		for (int i = 0; i < mods.size(); i++) {
			if (i < enabled.size())
				this->enabled.push_back(enabled.at(i));
			else
				this->enabled.push_back(false);
		}
	}
	emit layoutChanged();
}
