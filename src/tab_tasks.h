#ifndef TAB_TASKS_H
#define TAB_TASKS_H

#include <taskmodel.h>

#include <QWidget>
#include <QTreeView>
#include <QPushButton>

class tab_tasks : public QWidget {
	public:
		tab_tasks(QWidget * parent = nullptr);

		TaskModel * model = nullptr;

	public slots:
		void clearFinished();

	private:
		QTreeView * view_tasks = nullptr;
		QPushButton * button_clear = nullptr;
};

#endif //TAB_TASKS_H
