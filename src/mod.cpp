#include <mod.h>

#include <yaml-cpp/yaml.h>

#include <QString>
#include <QFile>
#include <QDir>

namespace mod {
	// TODO
	bool isModDirectory(std::string directory) {
		QString d_mod = QString::fromStdString(directory);
		return QFile(d_mod + "/main.lua").exists() && QFile(d_mod + "/metadata.json").exists();
	}

	ModPath getMod(std::string d_mod) {
		ModPath mod;
		QString path = QString::fromStdString(d_mod);
		if (QDir(path).exists() && isModDirectory(d_mod)) {
			mod.setPath(d_mod);
			mod.refresh();
		}
		return mod;
	}

	std::string Mod::getInfo(INFO info_type) const {
		switch (info_type) {
			case mod::INTERNALNAME:
				return internalname;
			case mod::NAME:
				return name;
			case mod::VERSION:
				return version.toString();
			case mod::AUTHOR:
				return author;
			/*
			case mod::TYPE:
				return "\0";
			case mod::SUMMARY:
				return "\0";
			*/
			case mod::DESCRIPTION:
				return description;
		}
		return "\0";
	}

	ModPath::ModPath() {}

	ModPath::ModPath(std::string path) {
		setPath(path);
	}

	bool ModPath::setPath(std::string path) {
		if (isModDirectory(path)) {
			this->path = path;
			return true;
		}
		return false;
	}

	bool ModPath::valid() const {
		return isModDirectory(path);
	}

	bool ModPath::refresh() {
		return read();
	}

	// Gets a string from the json representation
	std::string getJsonString(YAML::Node const & node, std::string const & name) {
		std::string value;
		if (node[name])
			value = node[name].as<std::string>();
		return value;
	}

	/*
	Regular JSON does not support trailing commas
	Most mods don't obey that hence YAML
	I would use nlohmann::json Qt or otherwise
	Also some values might not be read since json.net is case insensitive
	*/
	bool ModPath::read() {
		if (!valid())
			return false;

		QString p_mod = QString::fromStdString(path);
		QFile metadata(p_mod + "/metadata.json");
		if (!metadata.open(QIODevice::ReadOnly))
			return false;

		QString data = metadata.readAll();
		YAML::Node json = YAML::Load(data.toStdString());

		version      = version::SemanticVersion(getJsonString(json, "version"));
		internalname = getJsonString(json, "internalname");
		name         = getJsonString(json, "name");
		author       = getJsonString(json, "author");
		description  = getJsonString(json, "description");

		return true;
	}
}
