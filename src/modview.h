#ifndef MODVIEW_H
#define MODVIEW_H

#include <mod.h>

#include <QWidget>
#include <QLabel>
#include <QTextEdit>
#include <QPixmap>

class ModView : public QWidget {
	Q_OBJECT

	public:
		ModView(QWidget * parent = nullptr);

	public:
		void setMod(mod::IMod const & mod);
		void setIcon(QPixmap icon);

	private:
		QLabel * label_title;
		QLabel * label_icon;
		QTextEdit * text_description;
};

#endif //MODVIEW_H
